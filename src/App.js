import React from "react";
import { Route, BrowserRouter as Router } from 'react-router-dom'
import { ThemeProvider } from "styled-components";

import { theme } from "./theme";

import GlobalProvider from "./context/GlobalContext";
import GlobalStyle from "./styles/global";

import CharactersPage from "./pages/characters/index";
import CharacterPage from "./pages/character/index";
import Header from "./components/header/index";

const App = () => {
  return (
    <GlobalProvider>
      <ThemeProvider theme={theme}>
        <Router>
          <Header />
          <Route path="/" exact component={CharactersPage} />
          <Route path="/character/:id" component={CharacterPage} />
        </Router>
        <GlobalStyle />
      </ThemeProvider>
    </GlobalProvider>
  );
};

export default App;
