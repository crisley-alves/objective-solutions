import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
 * {
   margin: 0;
   padding: 0;
   outline: 0;
   box-sizing: border-box;
   font-family: 'PT Sans', sans-serif;
 }

 body {
   font-family: 'Roboto', sans-serif;
   background: #ffffff;
   color: #333;
   -webkit-font-smoothing: antialiased !important;
 }

 ul {
   list-style: none;
 }
`;
