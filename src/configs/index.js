const API_KEY = '&apikey=e987c2d8e1c6d7290bebea07a090ac8f&hash=4ce0ed723205564788bcfa1d50c76285'

export const CHARACTERS_URL = `http://gateway.marvel.com/v1/public/characters?ts=1${API_KEY}`;

export const CHARACTER_URL = `http://gateway.marvel.com/v1/public/characters/[id]?ts=1${API_KEY}`;

export const CHARACTER_SERIES_URL = `http://gateway.marvel.com/v1/public/characters/[id]/series?ts=1${API_KEY}`

export const CHARACTER_COMICS_URL = `http://gateway.marvel.com/v1/public/characters/[id]/comics?ts=1${API_KEY}`

export const CHARACTER_EVENTS_URL = `http://gateway.marvel.com/v1/public/characters/[id]/events?ts=1${API_KEY}`
