import { useCallback } from 'react';

const useFetch = () => {
  const fetchCallback = useCallback(async (url, params = {}) => {
    const urlWithParams = new URL(url);
    Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]))

    try {
      const response = await fetch(urlWithParams)
      const data = await response.json();
      return data;
    } catch (error) {
      console.error(error, 'error')
      return error;
    }
   }, []);
  
  return fetchCallback;
}

export default useFetch;