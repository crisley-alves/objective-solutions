import React, { useState, createContext } from 'react';

const initialState = {
  user: {
    name: 'Crisley Alves',
    photoUrl: 'https://avatars0.githubusercontent.com/u/23501772?s=460&u=f2b86e93e4cff4f8a7af63c0782f2c6dc0cb023a',
    email: 'crisleyalvesphx@gmail.com'
  },
}

export const GlobalContext = createContext();

const GlobalProvider = ({ children }) => {
  const [global, setGlobal] = useState(initialState);

  return (
    <GlobalContext.Provider value={[global, setGlobal]}>
      {children}
    </GlobalContext.Provider>
  )
}

export default GlobalProvider;