import styled from 'styled-components';

export const Page = styled.div.attrs({
  className: 'Page'
})`
  background-color: #E5E5E5;
`

export const Title = styled.h1.attrs({
  className: 'Title'
})`
  color: #555555;
  font-size: 32px;
  font-weight: 700;
  line-height: 30px;
`

export const Form = styled.form.attrs({
  className: 'Form'
})`
  margin: 30px 0;
  width: 100%;

  @media (min-width: 600px) {
    width: 50%;
  }

`;

export const Container = styled.div.attrs({
  className: 'Container'
})`
  background-color: #E5E5E5;
  width: 100%;
  padding: 15px;

  @media (min-width: 600px) {
    margin: 0 auto;
    width: 1200px;
    padding: 20px 0;
  }
`;
