import React, { useState, useEffect } from 'react';

import { CHARACTERS_URL } from '../../configs/index'
import { INITIAL_STATE } from '../utils/pagination';

import useCustomFetch from '../../hooks/useCustomFetch';
import InputComponent from '../../components/input/index';
import ListComponent from '../../components/list/index';
import PaginationComponent from '../../components/pagination/index';

import { Page, Container, Form, Title } from './styles'

const CharacterPage = ({
  history
}) => {
  const [pagination, setPagination] = useState(INITIAL_STATE)
  const [data, setData] = useState([])
  const [isLoading, setIsLoding] = useState(false)

  const fetch = useCustomFetch();

  const navigateToPage = (pageNumber, pages) => {
    setPagination({
      ...pagination,
      page: pageNumber,
      start: (pageNumber - 1) * pagination.limit,
      perPage: (pageNumber * pagination.limit),
      pages: pages !== undefined ? pages : Math.ceil(data.length / pagination.limit)
    });
  };

  const fetchCharacters = async (params) => {
    const queryParams = {
      limit: 50,
      ...params
    }

    setIsLoding(true);
    const { data: { results } } = await fetch(CHARACTERS_URL, queryParams);
    setIsLoding(false);
    setData(results)
    const pages = Math.ceil(results.length / pagination.limit)
    navigateToPage(1, pages);
  }

  useEffect(() => {
    fetchCharacters();
  }, []);

  async function searchByName(e) {
    e.preventDefault();
    const queryParams = {
      ...e.target.name.value && { nameStartsWith : e.target.name.value }
    };
    fetchCharacters(queryParams);
  }

  return (
    <Page>
      <Container>
        <Title>Busca de personagens</Title>

        <Form onSubmit={searchByName}>
          <InputComponent />
        </Form>

        <ListComponent
          data={data}
          isLoading={isLoading}
          pagination={pagination}
          navigateToPage={(id) => history.push(`character/${id}`)} />
      </Container>

      {pagination.pages > 0 && <PaginationComponent
        pagination={pagination}
        navigateToPage={navigateToPage}
      />
      }
    </Page>
  )
}

export default CharacterPage;
