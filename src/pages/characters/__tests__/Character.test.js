import { render, act, waitFor, fireEvent } from '@testing-library/react'

import CharacterPage from '../index';
import {
  mockAllCharactersData, mockSearchByNameNoResults, mockSearchCharactersByName,
} from './data';

// @TODO
// Move to a shared test util file
const mockFetchData = (mock) => {
  jest.spyOn(global, 'fetch').mockResolvedValue({
    json: jest.fn().mockResolvedValue(mock)
  });
};

describe('Character Page', () => {
  afterEach(() => jest.restoreAllMocks());

  test('Should render character page with data and pagination', async () => {
    mockFetchData(mockAllCharactersData);

    await act(async () => {
      const { getByText, findAllByTestId } = render(
        <CharacterPage />
      )
  
      const heroes = await waitFor(() => findAllByTestId('character-item'));
      const pageLinks = await waitFor(() => findAllByTestId('page-link'));
      const heroElement = await waitFor(() => getByText('Aaron Stack'));

      expect(heroes.length).toBe(10)
      expect(pageLinks.length).toBe(7)
      expect(heroElement.innerHTML).toBe('Aaron Stack')
    })
  })

  test('Should search hero by name and display results', async () => {
    mockFetchData(mockSearchCharactersByName)
    await act(async () => {
      const { findAllByTestId } = render(
        <CharacterPage />
      )

      const inputName = document.querySelector('#name');
      fireEvent.change(inputName, { target: { value: 'iron' } });
  
      const heroes = await waitFor(() => findAllByTestId('character-item'));
      const pageLinks = await waitFor(() => findAllByTestId('page-link'));

      expect(heroes.length).toBe(7)
      expect(pageLinks.length).toBe(1)
    })
  });

  test('Should search hero by name and display no results', async () => {
    mockFetchData(mockSearchByNameNoResults)
    await act(async () => {
      const { getByText } = render(
        <CharacterPage />
      )

      const inputName = document.querySelector('#name');
      fireEvent.change(inputName, { target: { value: 'doest not exist' } });
  
      const notFoundMessage = await waitFor(() => getByText('Infelizmente não encontramos nenhum herói. :('));

      expect(notFoundMessage).toBeInTheDocument();
    })
  });
})