import styled from 'styled-components';

export const Page = styled.div.attrs({
  className: 'Page'
})``

export const Container = styled.div.attrs({
  className: 'Container'
})`
  width: 100%;
  padding: 15px;

  @media (min-width: 600px) {
    margin: 0 auto;
    width: 1200px;
    padding: 20px 0;
  }
`;

export const Content = styled.div.attrs({
  className: 'Content'
})`
  display: flex;
  flex-direction: column;

  @media (min-width: 600px) {
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: 30px;
  }
`

export const Photo = styled.img.attrs({
  className: 'Photo'
})`
  width: 80%;
  height: 300px;
  margin: 0 auto;

  @media (min-width: 600px) {
    width: 50%;
    height: auto;
  }

`

export const BasicInformation = styled.div.attrs({
  clasName: 'BasicInformation'
})`
  color: #fff;
  display: flex;
  flex-direction: column;
  flex-basis: 50%;
  align-items: center;
  justify-content: center;
  margin-top: 15px;

  @media (min-width: 600px) {
    margin-top: 0;  
  }
`

export const Title = styled.h1.attrs({
  className: 'Title'
})`
  color: #E62429;
  font-size: 32px;
  font-weight: 700;
  text-transform: uppercase;
  text-align: left;
`

export const Overview = styled.div.attrs({
  className: 'Overview'
})`
  display: flex;
  flex-wrap: wrap;
`
