import React, { useState, useEffect } from 'react';

import useCustomFetch from '../../hooks/useCustomFetch';

import CharacterEvent from './components/character-event';
import Circle from './components/circle';

import {
  Container, Page, Content,
  Title, Photo, BasicInformation,
  Overview,
} from './styles';

import {
  CHARACTER_URL,
  CHARACTER_COMICS_URL,
  CHARACTER_SERIES_URL,
  CHARACTER_EVENTS_URL
} from '../../configs';

const CharacterPage = ({
  match: {
    params: {
      id
    }
  }
}) => {
  // @TODO
  // Maybe store list of characters in the context and get data from there, one less request
  const [characterData, setCharacterData] = useState(null);
  const [seriesData, setSeries] = useState([]);
  const [comicsData, setComics] = useState([]);
  const [eventsData, setEvents] = useState([]);

  const fetch = useCustomFetch();

  const fetchCharacter = async () => {
    const urlWithParams = CHARACTER_URL.replace('[id]', id);
    const { data: { results } } = await fetch(urlWithParams);
    const [ character ] = results;
    setCharacterData(character);
  }

  useEffect(() => {
    fetchCharacter();
    fetchSeries();
    fetchComics();
    fetchEvents();
  }, [])

  const fetchSeries = async () => {
    const urlWithParams = CHARACTER_SERIES_URL.replace('[id]', id);
    const { data: { results } } = await fetch(urlWithParams);
    setSeries(results);
  }

  const fetchComics = async () => {
    const urlWithParams = CHARACTER_COMICS_URL.replace('[id]', id);
    const { data: { results } } = await fetch(urlWithParams);
    setComics(results);
  }

  const fetchEvents = async () => {
    const urlWithParams = CHARACTER_EVENTS_URL.replace('[id]', id);
    const { data: { results } } = await fetch(urlWithParams);
    setEvents(results);
  }

  useEffect(() => {
    fetchCharacter();
  }, [])

  if(!characterData) return ('');

  const {
    thumbnail, name, comics,
    series, events, stories
  } = characterData;

  const characterPhoto = `${thumbnail.path}.${thumbnail.extension}`;

  return (
    <Page>
      <Container>
        <Content>
          <Photo src={characterPhoto} />
          <BasicInformation>
            <Title>{name}</Title>
            <Overview>

              <Circle title="Séries" number={series.items.length} />
              <Circle title="Comics" number={comics.items.length} />
              <Circle title="Events" number={events.items.length} />
              <Circle title="Stories" number={stories.items.length} />

            </Overview>
          </BasicInformation>
        </Content>

        {seriesData.length > 0 && <CharacterEvent title="Series" data={seriesData} />}
        {comicsData.length > 0 && <CharacterEvent title="Comics" data={comicsData} />}
        {eventsData.length > 0 && <CharacterEvent title="Eventos" data={eventsData} />}

      </Container>
    </Page>
  )
}

export default CharacterPage;