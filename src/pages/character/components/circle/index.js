import React from 'react';

import { Circle, OverviewItem, Text } from './styles';

const CircleComponent = ({
  title,
  number
}) => (
  <OverviewItem>
    <Text>{title}</Text>
    <Circle>{number}</Circle>
  </OverviewItem>
)

export default CircleComponent;
