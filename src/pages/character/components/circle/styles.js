import styled from 'styled-components';

export const OverviewItem = styled.div.attrs({
  className: 'OverviewItem'
})`
  display: flex;
  flex-direction: column;
  width: 50%;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  margin-bottom: 15px;
`

export const Circle = styled.div.attrs({
  className: 'Circle'
})`
  color: #000;
  align-items: center;
  border-radius: 50%;
  border: 8px solid #E62429;;
  display: flex;
  font-size: 30px;
  font-weight: 900;
  justify-content: center;
  text-align: center;
  width: 100px;
  height: 100px;
`

export const Text = styled.p.attrs({
  className: 'Text'
})`
  color: #000;
  font-size: 25px;
  text-align: center;
  margin-bottom: 10px;
`
