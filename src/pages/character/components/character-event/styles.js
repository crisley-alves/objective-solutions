import styled from 'styled-components';

export const CharacterEventList = styled.ul.attrs({
  className: 'CharacterEventList'
})`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 30px;

  @media (min-width: 600px) {
    justify-content: flex-start;
  }
`

export const EventTitle = styled.h2.attrs({
  className: 'EventTitle'
})`
  position: relative;

  &::after {
    background-color: #e62429;
    bottom: -10px;
    content: '';
    height: 5px;
    left: 0;
    width: 45px;
    position: absolute;
  }
`

export const CharacterEventListItem = styled.div.attrs({
  className: 'CharacterEventListItem'
})`
  flex-basis: 43%;
  cursor: pointer;
  margin: 0% 6% 2% 0%;
  text-align: center;
  transition: 0.5s;

  @media (min-width: 600px) {
    flex-basis: 17%;
    margin: 0% 2% 2% 0%;
    opacity: 0.7;

    &:hover {
      opacity: 1;
      transition: 0.5s;
    }
  }
`

export const CharacterEventListPhoto = styled.img.attrs({
  className: 'CharacterEventListPhoto'
})`
  width: 100%;
  height: 200px;
`

export const CharacterEventListName = styled.div.attrs({
  className: 'CharacterListName'
})`

`
