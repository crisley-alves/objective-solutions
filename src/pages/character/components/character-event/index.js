import React from 'react';

import {
  CharacterEventListItem, CharacterEventList,
  CharacterEventListPhoto, CharacterEventListName,
  EventTitle,
} from './styles';

const CharacterEvent = ({
  title = 'Stories',
  data = [],
}) => (
  <>
    <EventTitle>{title}</EventTitle>
    <CharacterEventList>
      {data && data.map(event => {
        const { thumbnail, title, urls } = event;
        const [firstUrl] = urls;
        const { url: externalUrl } = firstUrl;

        const eventPhoto = `${thumbnail.path}.${thumbnail.extension}`;

        return (
          <CharacterEventListItem
            key={title}
            onClick={() => window.open(externalUrl, '_blank')}
          >
            <CharacterEventListPhoto src={eventPhoto} alt="" />
            <CharacterEventListName>{title}</CharacterEventListName>
          </CharacterEventListItem>
        );
      })}
    </CharacterEventList>
  </>
);

export default CharacterEvent;