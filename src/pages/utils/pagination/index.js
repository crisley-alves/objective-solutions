export const INITIAL_STATE = {
  limit: 10,
  total: 0,
  start: 0,
  page: 1,
  pages: 0,
  perPage: 10,
  showPrevButton: false,
  showFirstPageButton: false,
  showNextButton: false,
  showLastPageButton: false
};
