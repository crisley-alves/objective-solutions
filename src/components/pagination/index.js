import React from 'react';

import { Icon, PageLink, Pagination } from './styles';

const PaginationComponent = ({
  pagination,
  navigateToPage
}) => {
  return (
    <Pagination>
      {pagination.page > 2 &&
        <PageLink
          data-testid="page-link"
          onClick={(e) => {
            e.preventDefault();
            navigateToPage(1);
          }}
          className="arrow"
          href="#">
          <Icon className="fas fa-angle-double-left icon"></Icon>
        </PageLink>
      }

      {pagination.page > 1 &&
        <PageLink
          data-testid="page-link"
          onClick={(e) => {
            e.preventDefault();
            navigateToPage(pagination.page - 1);
          }}
          className="arrow"
          href="#">
          <Icon className="fas fa-angle-left icon"></Icon>
        </PageLink>
      }

      {[...Array(100)].slice(0, pagination.pages).map((x, i) =>
        <PageLink
          data-testid="page-link"
          key={i + 1}
          onClick={(e) => {
            e.preventDefault();
            navigateToPage(i + 1);
          }}
          className={`page-link ${pagination.page === i + 1 ? 'active' : ''}`}
          href="#">
          {i + 1}
        </PageLink>
      )}

      {pagination.page !== pagination.pages &&
        <PageLink
          data-testid="page-link"
          onClick={(e) => {
            e.preventDefault();
            navigateToPage(pagination.page + 1);
          }}
          className="arrow"
          href="#"
        >
          <Icon className="fas fa-angle-double-right icon"></Icon>
        </PageLink>
      }

      {pagination.page < Math.ceil(pagination.pages / 2 + 1) && pagination.pages !== 1 &&
        <PageLink
          data-testid="page-link"
          onClick={(e) => {
            e.preventDefault();
            navigateToPage(pagination.pages);
          }}
          className="arrow"
          href="#">
          <Icon className="fas fa-angle-right icon"></Icon>
        </PageLink>
      }
    </Pagination>
  )
};

export default PaginationComponent;