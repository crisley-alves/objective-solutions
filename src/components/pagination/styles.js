import styled from 'styled-components';

export const Pagination = styled.div.attrs({
  className: 'Pagination'
})`
  background-color: #fff;
  display: flex;
  justify-content: center;
  margin-bottom: 15px;
  padding-top: 15px;
`

export const PageLink = styled.a.attrs({
  className: 'PageLink'
})`
  background-color: #F5F5F5;
  border-radius: 5px;
  border: 1px solid #E5E5E5;
  color: #555555;
  text-decoration: none;
  padding: 3px 13px;
  margin: 0 5px;

  &:hover {
    background-color: #167ABC;
    border: 1px solid #167ABC;
    color: #ffffff;

    &.arrow {
      background-color: #167ABC;
      border: 1px solid #167ABC;
      color: #ffffff;

      .icon {
        color: #ffffff;
      }
    }
  }

  &.active {
    color: #ffffff;
    background-color: #167ABC;
    border: 1px solid #167ABC;
  }

  &.arrow {
    background-color: transparent;
  }
`

export const Icon = styled.i.attrs({
  className: 'Icon'
})`
  color: #8E8E8E;
  font-weight: 300;
  font-size: 20px;
`