import React from 'react';

import {
  BasicInformation, Text, CharacterName,
  CharacterPhoto, Events, Item,
  List, ListHeader, Series,
  Message
} from './styles';

const ListComponent = ({
  header = ['Personagem', 'Série', 'Eventos'],
  data = [],
  isLoading = false,
  pagination,
  navigateToPage,
}) => {
  return (
    <>
      <ListHeader>
        {header.map(title => <Text key={title}>{title}</Text>)}
      </ListHeader>

      {isLoading && <Message>Carregando</Message>}
      {data.length === 0 && !isLoading && <Message>Infelizmente não encontramos nenhum herói. :(</Message>}

      <List>
        {!isLoading && data.slice(pagination.start, pagination.perPage).map(({
          id,
          name,
          thumbnail,
          series,
          events
        }) => {
          const { items: seriesItems } = series;
          const { items: eventsItems } = events;
          const characterPhoto = `${thumbnail.path}.${thumbnail.extension}`;
          

          return (
            <Item
              onClick={() => navigateToPage(id)}
              key={name} data-testid="character-item"
            >
              <BasicInformation>
                <CharacterPhoto src={characterPhoto} alt={name} />
                <CharacterName>{name}</CharacterName>
              </BasicInformation>

              <Series>
                {seriesItems.slice(0,3).map(({ name }, index) => <Text key={name + index}>{name}</Text>)}
              </Series>

              <Events>
                {eventsItems.slice(0,3).map(({ name }, index) => <Text key={name + index}>{name}</Text>)}
              </Events>
            </Item>
          )
        }
        )}
      </List>
    </>
  )
}

export default ListComponent;
