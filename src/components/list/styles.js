import styled from 'styled-components';

export const ListHeader = styled.div.attrs({
  className: 'ListHeader'
})`
  display: flex;
  flex-direction: row;
  padding: 0 15px;

  p {
    &:not(:first-child) {
      display: none;
    }
  
    @media (min-width: 600px) {
      &:not(:first-child) {
       display: initial; 
      }
    }
  }
`

export const Message = styled.h1.attrs({
  className: 'Message'
})`
  font-size: 1.2rem;
  text-align: center;
  margin-top: 20px
`

export const Text = styled.p.attrs({
  className: 'Text'
})`
  color: #8E8E8E;
  font-size: 12px;
  flex: 1;  
`

export const List = styled.ul.attrs({
  className: 'List'
})`
  margin-top: 10px;
`

export const Item = styled.li.attrs({
  className: 'Item'
})`
  background-color: #ffffff;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  flex: 1;
  margin-bottom: 10px;
  padding: 15px;

  &:hover {
    box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2);
  }
`

export const BasicInformation = styled.div.attrs({
  className: 'BasicInformation'
})`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const CharacterPhoto = styled.img.attrs({
  className: 'CharacterPhoto'
})`
  width: 50px;
  height: 50px;
`

export const CharacterName = styled.h2.attrs({
  className: 'CharacterName'
})`
  font-size: 16px;
  font-weight: 700;
  margin-left: 25px;
`

export const Series = styled.div.attrs({
  className: 'Series'
})`
  display: none;
  flex: 1;

  p {
    color: #555555;
    font-size: 14px;
    font-weight: 500;
    line-height: 16px;
  }

  @media (min-width: 600px) {
    display: initial;
  }
`

export const Events = styled.div.attrs({
  className: 'Events'
})`
  flex: 1;
  display: none;

  p {
    color: #555555;
    font-size: 14px;
    font-weight: 500;
    line-height: 16px;
  }

  @media (min-width: 600px) {
    display: initial;
  }
`