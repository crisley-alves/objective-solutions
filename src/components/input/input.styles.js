import styled from 'styled-components';

export const InputWrapper = styled.div.attrs({
  className: 'InputWrapper'
})`
  position: relative;
`

export const Label = styled.label.attrs({
  className: 'Label'
})`
  color: #555555;
  display: block;
  font-weight: 700;
`

export const Icon = styled.i.attrs({
  className: 'Icon'
})`
  color: #8E8E8E;
  font-size: 15px;
  font-weight: 700;
  position: absolute;
  left: auto;
  right: 20px;
  top: 40px;
  

  @media (min-width: 600px) {
    left: 239px;
    right: initial;
  }
`

export const Input = styled.input.attrs({
  className: 'Input'
})`
  border: 1px solid #E5E5E5;
  border-radius: 5px;
  margin-top: 12px;
  padding: 5px 10px;
  width: 100%;

  &::placeholder {
    color: #555555;
    font-style: italic;
  }

  @media (min-width: 600px) {
    width: 45%;
  }

`
