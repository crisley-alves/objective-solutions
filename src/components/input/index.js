import { Icon, Input, InputWrapper, Label } from './input.styles'

const InputComponent = ({
  label = 'Nome do Personagem',
  icon = 'fas fa-search icon',
  type = 'text',
  placeholder = 'Search',
  id = 'name',
  name = 'name'
}) => (
  <InputWrapper>
    <Label htmlFor={name}>{label}</Label>
    <Icon className={icon}></Icon>
    <Input type={type} placeholder={placeholder} id={id} name={name} />
  </InputWrapper>
);

export default InputComponent;
