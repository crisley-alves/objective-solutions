import styled from 'styled-components';

export const Header = styled.header.attrs({
  className: 'Header'
})`
`

export const Container = styled.div.attrs({
  className: 'Container'
})`
  width: 100%;

  @media (min-width: 600px) {
    margin: 0 auto;
    width: 1200px;
  }
`

export const Content = styled.div.attrs({
  className: 'Content'
})`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 0;
`

export const Logo = styled.img.attrs({
  className: 'Logo'
})`
  width: 100px;
  height: 20px;
`

export const Account = styled.div.attrs({
  className: 'Account'
})`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const Text = styled.p.attrs({
  className: 'Text'
})`
  display: inline-block;
  color: #555555;
  font-weight: 600;
  font-size: 12px;
  text-align: right;

  span {
    display: block;
    font-weight: 500;
  }

  @media (min-width: 600px) {
    font-size: 14px;

    span {
      display: initial;
    }
  }
`

export const Employee = styled.img.attrs({
  className: 'Employee'
})`
  width: 50px;
  border-radius: 50%;
  margin-left: 10px;
`