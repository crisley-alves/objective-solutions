import React, { useContext } from 'react';

import CompanyLogo from '../../images/logo.png';

import { GlobalContext } from '../../context/GlobalContext';

import {
  Account, Content, Employee,
  Header, Logo, Text, Container
} from './styles';

const HeaderComponent = () => {
  const [global] = useContext(GlobalContext);

  return (
    <Header>
      <Container>
        <Content>
          <Logo src={CompanyLogo} alt="logo empresa" />
  
          <Account>
            <Text>{global.user.name} <span>Teste de Front-End</span></Text>
            <Employee src={global.user.photoUrl} alt="" />
          </Account>
        </Content>
      </Container>
    </Header>
  );
};
  
export default HeaderComponent;